<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChampionOriginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champion_origin', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('champion_id')->unsigned();
            $table->foreign('champion_id')->references('id')->on('champions')->nullable();
            $table->integer('origin_id')->unsigned();
            $table->foreign('origin_id')->references('id')->on('origins')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champion_origin');
    }
}
