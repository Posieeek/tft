<?php

use Illuminate\Database\Seeder;
use App\Champion;
use App\Type;

class ChampionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Shyvana')->first()->id,
            'type_id' => Type::where('name', 'Shapeshifter')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Aurelion Sol')->first()->id,
            'type_id' => Type::where('name', 'Sorcerer')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Pantheon')->first()->id,
            'type_id' => Type::where('name', 'Guardian')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Aatrox')->first()->id,
            'type_id' => Type::where('name', 'Blademaster')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Brand')->first()->id,
            'type_id' => Type::where('name', 'Elementalist')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Elise')->first()->id,
            'type_id' => Type::where('name', 'Shapeshifter')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Evelynn')->first()->id,
            'type_id' => Type::where('name', 'Assassin')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Morgana')->first()->id,
            'type_id' => Type::where('name', 'Sorcerer')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Swain')->first()->id,
            'type_id' => Type::where('name', 'Shapeshifter')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Varus')->first()->id,
            'type_id' => Type::where('name', 'Ranger')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Yasuo')->first()->id,
            'type_id' => Type::where('name', 'Blademaster')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Anivia')->first()->id,
            'type_id' => Type::where('name', 'Elementalist')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Ashe')->first()->id,
            'type_id' => Type::where('name', 'Ranger')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Braum')->first()->id,
            'type_id' => Type::where('name', 'Guardian')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Lissandra')->first()->id,
            'type_id' => Type::where('name', 'Elementalist')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Sejuani')->first()->id,
            'type_id' => Type::where('name', 'Knight')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Volibear')->first()->id,
            'type_id' => Type::where('name', 'Brawler')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Camille')->first()->id,
            'type_id' => Type::where('name', 'Blademaster')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Jayce')->first()->id,
            'type_id' => Type::where('name', 'Shapeshifter')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Vi')->first()->id,
            'type_id' => Type::where('name', 'Brawler')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Jinx')->first()->id,
            'type_id' => Type::where('name', 'Gunslinger')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Darius')->first()->id,
            'type_id' => Type::where('name', 'Knight')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Draven')->first()->id,
            'type_id' => Type::where('name', 'Blademaster')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Katarina')->first()->id,
            'type_id' => Type::where('name', 'Assassin')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Fiora')->first()->id,
            'type_id' => Type::where('name', 'Blademaster')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Garen')->first()->id,
            'type_id' => Type::where('name', 'Knight')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Kayle')->first()->id,
            'type_id' => Type::where('name', 'Knight')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Leona')->first()->id,
            'type_id' => Type::where('name', 'Guardian')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Lucian')->first()->id,
            'type_id' => Type::where('name', 'Gunslinger')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Vayne')->first()->id,
            'type_id' => Type::where('name', 'Ranger')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Akali')->first()->id,
            'type_id' => Type::where('name', 'Assassin')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Kennen')->first()->id,
            'type_id' => Type::where('name', 'Elementalist')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Shen')->first()->id,
            'type_id' => Type::where('name', 'Blademaster')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Zed')->first()->id,
            'type_id' => Type::where('name', 'Assassin')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Karthus')->first()->id,
            'type_id' => Type::where('name', 'Sorcerer')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Kindred')->first()->id,
            'type_id' => Type::where('name', 'Ranger')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Mordekaiser')->first()->id,
            'type_id' => Type::where('name', 'Knight')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Gangplank')->first()->id,
            'type_id' => Type::where('name', 'Blademaster')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Gangplank')->first()->id,
            'type_id' => Type::where('name', 'Gunslinger')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Graves')->first()->id,
            'type_id' => Type::where('name', 'Gunslinger')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Miss Fortune')->first()->id,
            'type_id' => Type::where('name', 'Gunslinger')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Pyke')->first()->id,
            'type_id' => Type::where('name', 'Assassin')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Twisted Fate')->first()->id,
            'type_id' => Type::where('name', 'Sorcerer')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Blitzcrank')->first()->id,
            'type_id' => Type::where('name', 'Brawler')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', "Cho'Gath")->first()->id,
            'type_id' => Type::where('name', 'Brawler')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Kassadin')->first()->id,
            'type_id' => Type::where('name', 'Sorcerer')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', "Kha'Zix")->first()->id,
            'type_id' => Type::where('name', 'Assassin')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', "Rek'Sai")->first()->id,
            'type_id' => Type::where('name', 'Brawler')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Ahri')->first()->id,
            'type_id' => Type::where('name', 'Sorcerer')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Gnar')->first()->id,
            'type_id' => Type::where('name', 'Shapeshifter')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Nidalee')->first()->id,
            'type_id' => Type::where('name', 'Shapeshifter')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Rengar')->first()->id,
            'type_id' => Type::where('name', 'Assassin')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Warwick')->first()->id,
            'type_id' => Type::where('name', 'Brawler')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Lulu')->first()->id,
            'type_id' => Type::where('name', 'Sorcerer')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Poppy')->first()->id,
            'type_id' => Type::where('name', 'Knight')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Tristana')->first()->id,
            'type_id' => Type::where('name', 'Gunslinger')->first()->id,
        ]);

        DB::table('champion_type')->insert([
            'champion_id' => Champion::where('name', 'Veigar')->first()->id,
            'type_id' => Type::where('name', 'Sorcerer')->first()->id,
        ]);



       
        //

    }
}
