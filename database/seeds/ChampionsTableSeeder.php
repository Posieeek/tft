<?php

use Illuminate\Database\Seeder;
use App\Type;
use App\Champion;

class ChampionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
      
    

        // Dragon 1-3
    /*    DB::table('champions')->insert([
            'name' => 'Shyvana',
            'type_id'=> Type::where('name', 'Shapeshifter')->first()->id,
            'description' => 'Bije',
            'price' => '3',
        ]);*/

        DB::table('champions')->insert([
            'name' => 'Shyvana',
            'description' => 'Shyvana dashes away and transforms. While transformed, Shyvana attacks become ranged and light the ground on fire.
            Burn Damage: 200 / 300 / 400
            Burn Duration: 3
            Transform Duration: 60
            Transform Damage: 100 / 150 / 200
            Mana Cost: 85
            Starting Mana: 0',
            'price' => '3',
            'health' => 650,
            'armor' => 30,
            'mr' => 20,
            'dps' => 35,
        ]);

        DB::table('champions')->insert([
            'name' => 'Aurelion Sol',
            'description' => 'Aurelion Sol breathes a large blast of fire in a line, dealing damage to enemies.
            Damage
            250 / 500 / 750
            Channel Duration
            0.35s',
            'price' => '4',
            'health' => 650,
            'armor' => 20,
            'mr' => 20,
            'dps' => 24,
        ]);

        DB::table('champions')->insert([
            'name' => 'Pantheon',
            'description' => 'Pantheon leaps in the air, becomes untargetable, and crashes down towards the farthest enemy stunning them. As he lands, Pantheon deals magic damage to all enemies in his path based of their Maximum Health. They then burn for an additional percentage of their Maximum Health as True Damage over a few seconds and applies Grievous Wounds.
            Damage
            10% / 20% 30% Max HP
            Stun Duration
            2s / 2s / 2s',
            'price' => '5',
            'health' => 850,
            'armor' => 80,
            'mr' => 20,
            'dps' => 56,
        ]);


        //Demon 4-11
        DB::table('champions')->insert([
            'name' => 'Aatrox',
            'description' => 'Aatrox cleaves the area in front of him, dealing damage to enemies inside it.
            Damage
            300 / 600 / 900',
            'price' => '3',
            'health' => 700,
            'armor' => 25,
            'mr' => 20,
            'dps' => 42,
        ]);

        DB::table('champions')->insert([
            'name' => 'Brand',
            'description' => 'Brand launches a bouncing fireball, damaging enemies hit.
            Damage
            250 / 450 / 650
            Bounces
            4 / 4 / 6',
            'price' => '4',
            'health' => 700,
            'armor' => 25,
            'mr' => 20,
            'dps' => 36,
        ]);

        DB::table('champions')->insert([
            'name' => 'Elise',
            'description' => 'Elise fires a cocoon stunning the nearest enemy and transforms, summoning 2 Spiderlings.
            Life Steal
            60% / 90% / 120%
            Number of Spiderlings
            1 / 2 / 4
            Spiderling Attack Damage
            60
            Spiderling Attack Speed
            0.7
            Spiderling Health
            500',
            'price' => '1',
            'health' => 500,
            'armor' => 20,
            'mr' => 20,
            'dps' => 27,
        ]);

        DB::table('champions')->insert([
            'name' => 'Evelynn',
            'description' => 'Evelynn deals damage to the 3 closest enemies and teleports away. Damage is increased against low health enemies.
            Damage
            200 / 300 / 400
            Crit Multiplier
            3x / 4x / 5x
            Crit Threshold
            50% HP
            Blink Distance
            3',
            'price' => '3',
            'health' => 550,
            'armor' => 20,
            'mr' => 20,
            'dps' => 42,
        ]);

        DB::table('champions')->insert([
            'name' => 'Morgana',
            'description' => 'Morgana fires chains to nearby enemies, dealing damage and stunning after a short delay if they are still nearby.
            Damage
            175 / 300 / 425
            Chain Duration
            3s
            Stun Duration
            2s / 4s / 6s',
            'price' => '3',
            'health' => 650,
            'armor' => 30,
            'mr' => 20,
            'dps' => 30,
        ]);

        DB::table('champions')->insert([
            'name' => 'Swain',
            'description' => 'Swain transforms, draining health from all nearby enemies.
            Soulflare Damage
            300 / 600 / 900
            Damage Per Tick
            50 / 100 / 150
            Heal Per Tick
            50 / 90 / 130
            Transform Duration
            6s',
            'price' => '5',
            'health' => 850,
            'armor' => 25,
            'mr' => 20,
            'dps' => 42,
        ]);

        DB::table('champions')->insert([
            'name' => 'Varus',
            'description' => 'Varus charges and fires an arrow, dealing damage to all enemies in a line.
            Damage
            300 / 550 / 800
            Cast Time
            1.5s',
            'price' => '2',
            'health' => 500,
            'armor' => 25,
            'mr' => 20,
            'dps' => 35,
        ]);

        //Exile 12

        DB::table('champions')->insert([
            'name' => 'Yasuo',
            'description' => 'Yasuo stabs forward dealing damage. On third cast, Yasuo launches a tornado dealing damage and knocking up enemies in a line.
            Damage
            150 / 350 / 550
            Range
            2
            Empowered Range
            4 hexes
            Knockup Duration
            1.55s',
            'price' => '5',
            'health' => 700,
            'armor' => 35,
            'mr' => 20,
            'dps' => 75,
        ]);

        //Glacial 13-18

        DB::table('champions')->insert([
            'name' => 'Anivia',
            'description' => 'Anivia channels a large hailstorm, damaging enemies inside of it.
            Damage
            800 / 950 / 1200
            Attack Speed Slow
            50 / 70 / 90
            Storm Duration
            6s',
            'price' => '5',
            'health' => 700,
            'armor' => 20,
            'mr' => 20,
            'dps' => 24,
        ]);

        DB::table('champions')->insert([
            'name' => 'Ashe',
            'description' => 'Ashe fires an arrow that travels across the map, damages, and stuns (stun duration based on each hex traveled).
            Damage
            700 / 950 / 1200
            Stun Duration (per hex traveled)
            1s / 1.5s / 2s',
            'price' => '3',
            'health' => 550,
            'armor' => 20,
            'mr' => 20,
            'dps' => 46,
        ]);

        DB::table('champions')->insert([
            'name' => 'Braum',
            'description' => 'Braum creates a barrier that blocks all incoming damage.
            Damage Reduction
            70% / 80% / 90%
            Duration
            4s',
            'price' => '2',
            'health' => 650,
            'armor' => 75,
            'mr' => 20,
            'dps' => 24,
        ]);

        DB::table('champions')->insert([
            'name' => 'Lissandra',
            'description' => 'Lissandra encases the target in ice, dealing damage to nearby enemies. Below half HP, Lissandra instead encases herself, becoming untargetable.
            Damage
            175 / 325 / 475
            Enemy Stun Duration
            1.5s
            Untargetable Duration
            2s
            Slow Duration
            3s
            Slow Field Duration
            3s',
            'price' => '2',
            'health' => 500,
            'armor' => 20,
            'mr' => 20,
            'dps' => 24,
        ]);

        DB::table('champions')->insert([
            'name' => 'Sejuani',
            'description' => 'Sejuani creates a large glacial storm, stunning enemies within it after a short delay.
            Damage
            100 / 175 / 250
            Stun Duration
            2s / 4s / 6s',
            'price' => '4',
            'health' => 850,
            'armor' => 40,
            'mr' => 25,
            'dps' => 25,
        ]);

        DB::table('champions')->insert([
            'name' => 'Volibear',
            'description' => "Volibear's attacks bounce between enemies.
            Chain Damage Multiplier
            80% / 90% / 100%
            Bounces
            3 / 4 / 5",
            'price' => '3',
            'health' => 700,
            'armor' => 30,
            'mr' => 20,
            'dps' => 41,
        ]);

        // Hextech 19-22

        DB::table('champions')->insert([
            'name' => 'Camille',
            'description' => "Camille singles out an enemy, dealing magic damage and rooting them for few seconds. Camille's allies in range will prioritize attacking that enemy.
            Damage
            200 / 325 / 450
            Root Duration
            4s / 5s / 6s",
            'price' => '1',
            'health' => 550,
            'armor' => 25,
            'mr' => 20,
            'dps' => 33,
        ]);

        DB::table('champions')->insert([
            'name' => 'Jayce',
            'description' => 'Jayce knocks away an emeny in melee range, dealing damage and stunning them for few seconds. He then transforms his hammer into a cannon, increasing his attack range by 3 hexes and gaining max attack speed for few attacks.
            Damage
            200 / 350 / 500
            Stun Duration
            2.5s / 4.25s / 6s
            Attacks
            3 / 5 / 7',
            'price' => '2',
            'health' => 600,
            'armor' => 35,
            'mr' => 20,
            'dps' => 39,
        ]);

        DB::table('champions')->insert([
            'name' => 'Vi',
            'description' => 'Vi charges down the furthest enemy, knocking aside anyone in her way. When she reaches her target she deals magic damage and knocks them up. Other enemies knocked aside take the same damage.
            Damage
            250 / 450 / 650
            Stun Duration
            2s / 2.5s / 3s',
            'price' => '3',
            'health' => 600,
            'armor' => 25,
            'mr' => 20,
            'dps' => 36,

        ]);

        DB::table('champions')->insert([
            'name' => 'Jinx',
            'description' => 'Jinx gets excited as she participates in kills. On her first takedown, she gains attack speed. On her second, she swaps to her rocket launcher causing her attacks to deal bonus area damage.
            Attack Speed
            60% / 80% / 100%
            Damage
            100 / 200 / 300',
            'price' => '4',
            'health' => 550,
            'armor' => 20,
            'mr' => 20,
            'dps' => 49,
        ]);

        // Imperial without Swain 23-25

        DB::table('champions')->insert([
            'name' => 'Darius',
            'description' => 'Darius swings his axe, damaging nearby enemies and healing himself based off his missing health.
            Damage
            150 / 225 / 300
            Heal
            100 / 150 / 200
            Delay
            1.5',
            'price' => '1',
            'health' => 600,
            'armor' => 40,
            'mr' => 20,
            'dps' => 25,
        ]);

        DB::table('champions')->insert([
            'name' => 'Draven',
            'description' => 'Draven gains bonus on-hit damage and Attack Speed. Stacks up to two times.
            Buff Duration
            8s
            Attack Damage Multiplier
            150% / 200% / 250%',
            'price' => '4',
            'health' => 650,
            'armor' => 25,
            'mr' => 20,
            'dps' => 53,


        ]);

        DB::table('champions')->insert([
            'name' => 'Katarina',
            'description' => 'Katarina channels and fires daggers at a number of nearby enemies, dealing damage and reducing healing.
            Damage
            600 / 900 / 1200
            Channel Duration
            2.5s
            Grievous Wounds Duration
            3s
            Targets
            4 / 6 / 8',
            'price' => '3',
            'health' => 450,
            'armor' => 20,
            'mr' => 20,
            'dps' => 39,
        ]);

    

        // Noble 26-31

        DB::table('champions')->insert([
            'name' => 'Fiora',
            'description' => 'Fiora becomes immune to damage and spells. After a short delay, she stuns and damages the closest enemy.
            Damage
            100 / 250 / 400
            Stun Duration
            1.5s
            Block Duration
            1.5s',
            'price' => '1',
            'health' => 400,
            'armor' => 25,
            'mr' => 20,
            'dps' => 40,
        ]);

        DB::table('champions')->insert([
            'name' => 'Garen',
            'description' => 'Garen rapidly spins his sword around his body, becoming immune to magic damage and dealing damage to nearby enemies.
            Total Damage
            360 / 585 / 810
            Damage per Tick
            50 / 65 / 80
            Total Ticks
            9
            Spin Duration
            4s',
            'price' => '1',
            'health' => 600,
            'armor' => 40,
            'mr' => 20,
            'dps' => 30,
        ]);

        DB::table('champions')->insert([
            'name' => 'Kayle',
            'description' => 'Kayle shields an ally, making them immune to damage.
            Shield Duration
            2s / 2.5s / 3s
            Extra Targets
            0 / 1 / 2',
            'price' => '5',
            'health' => 750,
            'armor' => 40,
            'mr' => 20,
            'dps' => 60,
        ]);

        DB::table('champions')->insert([
            'name' => 'Leona',
            'description' => 'Leona calls down a solar ray, stunning enemies in the center and dealing damage to enemies inside it.
            Damage
            175 / 250 / 325
            Stun Duration
            5s / 7s / 9s
            Stun Delay
            0.625s',
            'price' => '4',
            'health' => 750,
            'armor' => 100,
            'mr' => 20,
            'dps' => 25,
        ]);

        DB::table('champions')->insert([
            'name' => 'Lucian',
            'description' => 'Lucian dashes away to safety and attacks an enemy twice, once with Attack Damage and once with Spell Damage.
            Second Shot Damage
            100 / 225 / 350',
            'price' => '2',
            'health' => 600,
            'armor' => 25,
            'mr' => 20,
            'dps' => 42,
        ]);

        DB::table('champions')->insert([
            'name' => 'Vayne',
            'description' => "Vayne deals bonus true damage every third attack based on the enemy's maximum health.
            Max Health Damage
            8% / 12% / 16%",
            'price' => '1',
            'health' => 550,
            'armor' => 25,
            'mr' => 20,
            'dps' => 28,
        ]);

        // Ninja 32-35

        DB::table('champions')->insert([
            'name' => 'Akali',
            'description' => 'Akali throws shurikens in front of her, dealing damage.
            Damage
            200 / 350 / 500',
            'price' => '4',
            'health' => 650,
            'armor' => 20,
            'mr' => 20,
            'dps' => 56,
        ]);

        DB::table('champions')->insert([
            'name' => 'Kennen',
            'description' => 'Kennen summons a storm around him, dealing damage and stunning enemies inside of it.
            Damage
            225 / 450 / 675
            Stun Duration
            1.5s',
            'price' => '3',
            'health' => 550,
            'armor' => 20,
            'mr' => 20,
            'dps' => 42,
        ]);

        DB::table('champions')->insert([
            'name' => 'Shen',
            'description' => 'Shen creates a zone around himself, allowing allies to dodge all attacks.
            Zone Duration
            3s / 4s / 5s',
            'price' => '2',
            'health' => 700,
            'armor' => 30,
            'mr' => 20,
            'dps' => 46,
        ]);

        DB::table('champions')->insert([
            'name' => 'Zed',
            'description' => 'Zed fires a shuriken in a line, damaging enemies it passes through.
            Damage
            200 / 350 / 500
            Range
            4',
            'price' => '2',
            'health' => 550,
            'armor' => 25,
            'mr' => 20,
            'dps' => 46,
        ]);

        // Phantom 36-38

        DB::table('champions')->insert([
            'name' => 'Karthus',
            'description' => 'Karthus deals damage to a number of random enemies after a long channel.
            Damage
            350 / 600 / 850
            Targets
            5 / 7 / 9',
            'price' => '5',
            'health' => 850,
            'armor' => 25,
            'mr' => 20,
            'dps' => 42,
        ]);

        DB::table('champions')->insert([
            'name' => 'Kindred',
            'description' => 'Kindred creates a zone around herself that prevents allies from dying.
            Duration
            4s / 5s / 6s
            Minimum HP
            300 / 600 / 900',
            'price' => '4',
            'health' => 600,
            'armor' => 20,
            'mr' => 20,
            'dps' => 39,
        ]);

        DB::table('champions')->insert([
            'name' => 'Mordekaiser',
            'description' => 'Mordekaiser slams his mace in front him, dealing damage in a line.
            Damage
            150 / 250 / 350
            Start Distance
            2
            Length
            3',
            'price' => '1',
            'health' => 550,
            'armor' => 40,
            'mr' => 20,
            'dps' => 25,
        ]);

        //Pirates 39-43

        DB::table('champions')->insert([
            'name' => 'Gangplank',
            'description' => 'Gangplank periodically creates barrels. On cast, Gangplank detonates the barrels, damaging nearby enemies.
            Damage
            150 / 250 / 350',
            'price' => '3',
            'health' => 700,
            'armor' => 20,
            'mr' => 20,
            'dps' => 36,
        ]);

        DB::table('champions')->insert([
            'name' => 'Graves',
            'description' => "Graves' attacks deal splash damage to nearby enemies.
            Damage Multiplier
            0.1 / 0.2 / 0.3",
            'price' => '1',
            'health' => 450,
            'armor' => 30,
            'mr' => 20,
            'dps' => 30,
        ]);

        DB::table('champions')->insert([
            'name' => 'Miss Fortune',
            'description' => 'Miss Fortune channels and fires several waves of bullets in a cone.
            Channel Duration
            3s
            Total Damage
            900 / 1300 / 1700',
            'price' => '5',
            'health' => 700,
            'armor' => 20,
            'mr' => 20,
            'dps' => 64,
        ]);

        DB::table('champions')->insert([
            'name' => 'Pyke',
            'description' => 'Pyke dashes behind the furthest enemy, creating an afterimage that stuns enemies it passes through.
            Damage
            150 / 200 / 250
            Stun Duration
            1.5s / 2s / 2.5s
            Stun Delay
            1',
            'price' => '2',
            'health' => 600,
            'armor' => 25,
            'mr' => 20,
            'dps' => 36,
        ]);

        DB::table('champions')->insert([
            'name' => 'Twisted Fate',
            'description' => 'Twisted Fate throws a card that either stuns, deals damage around his target, or restores mana to himself and nearby allies.
            Red Card AoE Damage
            150 / 250 / 350
            Yellow Card Stun Duration
            2s / 3s / 4s
            Blue Card Mana Restore
            30 / 50 / 70',
            'price' => '2',
            'health' => 500,
            'armor' => 20,
            'mr' => 20,
            'dps' => 28,
        ]);

        // 44 Robot

        DB::table('champions')->insert([
            'name' => 'Blitzcrank',
            'description' => 'Blitzcrank pulls the furthest enemy to him.
            Damage
            100 / 350 / 800
            Stun Duration
            2.5s',
            'price' => '2',
            'health' => 600,
            'armor' => 35,
            'mr' => 20,
            'dps' => 25,
        ]);

        // 45-48 Void

        DB::table('champions')->insert([
            'name' => "Cho'Gath",
            'description' => "Cho'gath ruptures an area, stunning and damaging enemies inside of it.
            Damage
            175 / 350 / 525
            Knockup Duration
            1.5s / 1.75s / 2s
            AoE Radius
            3 hexes
            Delay
            1.5s",
            'price' => '4',
            'health' => 1000,
            'armor' => 20,
            'mr' => 20,
            'dps' => 42,
        ]);

        DB::table('champions')->insert([
            'name' => 'Kassadin',
            'description' => "Kassadin's attacks steal mana from enemies, converting it into a shield.
            Shield Duration
            4s
            Mana Steal
            20 / 40 / 60",
            'price' => '1',
            'health' => 550,
            'armor' => 25,
            'mr' => 20,
            'dps' => 30,
        ]);

        DB::table('champions')->insert([
            'name' => "Kha'Zix",
            'description' => "Kha'Zix slashes the closest enemy, dealing bonus damage to enemies that are alone.
            Damage
            150 / 250 / 350
            Isolation Damage
            400 / 600 / 800",
            'price' => '1',
            'health' => 500,
            'armor' => 20,
            'mr' => 20,
            'dps' => 33,
        ]);

        DB::table('champions')->insert([
            'name' => "Rek'Sai",
            'description' => "Rek'Sai burrows for a short duration becoming untargetable and healing. When Rek'Sai unburrows she deals damage and knocks up the closest enemy.
            Damage
            200 / 350 / 500
            Heal Amount
            150 / 300 / 450
            Burrow Duration
            1s
            Knockup Duration
            1.75s",
            'price' => '2',
            'health' => 650,
            'armor' => 20,
            'mr' => 20,
            'dps' => 33,
        ]);

        // Wild 49-53 without 

        DB::table('champions')->insert([
            'name' => 'Ahri',
            'description' => 'Ahri fires an orb in a line that returns to her, damaging enemies it passes through.
            Damage
            100 / 200 / 300',
            'price' => '2',
            'health' => 450,
            'armor' => 20,
            'mr' => 20,
            'dps' => 28,
        ]);

        DB::table('champions')->insert([
            'name' => 'Gnar',
            'description' => 'Gnar transforms and jumps behind the furthest enemy, damaging and shoving enemies backwards.
            Damage
            200 / 300 / 400
            CC Duration
            2s
            Knockback Distance
            2 hexes
            Transform Duration
            60s
            Transform Bonus HP
            300 / 600 / 900
            Transform Bonus Attack Damage
            50 / 100 / 150',
            'price' => '4',
            'health' => 750,
            'armor' => 30,
            'mr' => 20,
            'dps' => 35,
        ]);

        DB::table('champions')->insert([
            'name' => 'Nidalee',
            'description' => 'Nidalee heals herself and two allies, then transforms.
            HoT total heal
            150 / 225 / 600
            HoT Duration
            6s
            Transform Damage
            20 / 65 / 120',
            'price' => '1',
            'health' => 500,
            'armor' => 20,
            'mr' => 20,
            'dps' => 33,
        ]);

        DB::table('champions')->insert([
            'name' => 'Rengar',
            'description' => 'Rengar leaps to the weakest enemy and stabs them.
            Damage Multiplier
            2.1 / 3.2 / 4.3
            Attack Speed
            30% / 50% / 70%
            Crit Chance
            25%
            Buff Duration
            6s',
            'price' => '3',
            'health' => 550,
            'armor' => 20,
            'mr' => 20,
            'dps' => 42,
        ]);

        DB::table('champions')->insert([
            'name' => 'Warwick',
            'description' => 'Warwick pounces onto the lowest health enemy, stunning and damaging them.
            Damage
            150 / 225 / 300
            Hits
            3
            Duration
            1.5s',
            'price' => '1',
            'health' => 650,
            'armor' => 30,
            'mr' => 20,
            'dps' => 30,
        ]);

        // 54-57 without Kennen, Gnar

        DB::table('champions')->insert([
            'name' => 'Lulu',
            'description' => 'Lulu grants an ally bonus Health, knocking up enemies near them.
            Bonus HP
            300 / 400 / 500
            Extra Targets
            0 / 1 / 2
            Duration
            6s
            Knockup Duration
            1.25s',
            'price' => '2',
            'health' => 500,
            'armor' => 20,
            'mr' => 20,
            'dps' => 30,
        ]);

        DB::table('champions')->insert([
            'name' => 'Poppy',
            'description' => 'Poppy brings down her hammer, knocking away nearby enemies.
            Damage
            300 / 500 / 700
            Knockup Duration
            1s
            Stun Duration
            2s / 2s / 2s
            Number of Targets Hit
            1 / 2 / 3',
            'price' => '3',
            'health' => 800,
            'armor' => 40,
            'mr' => 20,
            'dps' => 25,
        ]);

        DB::table('champions')->insert([
            'name' => 'Tristana',
            'description' => 'Tristana places a bomb on her current target that detonates after 4 attacks, damaging nearby enemies.
            Charge Damage
            150 / 225 / 300
            Charge Duration
            4s',
            'price' => '1',
            'health' => 500,
            'armor' => 20,
            'mr' => 20,
            'dps' => 33,
        ]);

        DB::table('champions')->insert([
            'name' => 'Veigar',
            'description' => 'Veigar blasts an enemy with magical energy. This spell instantly kills if the enemy is a lower star level than Veigar.
            Damage
            300 / 550 / 800',
            'price' => '3',
            'health' => 500,
            'armor' => 20,
            'mr' => 20,
            'dps' => 25,
        ]);

        $champions = Champion::all();
        foreach ($champions as $champion) {

            $champion->update(['avatar' => "$champion->name.png"]);
        }

    }
}
