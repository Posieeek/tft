<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    
/*Akali
Evelynn
Katarina
Kha'Zix
Pyke
Rengar
Zed*/
    DB::table('types')->insert([
        'id' => '1',
        'name' => 'Assassin',
        'description' => 'Assassins leap to the farthest enemy at the start of combat and deal additional Critical Strike Damage.',
        'updated_at' => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
    ]);  
/* Aatrox
Camille
Draven
Fiora
Gangplank
Shen
Yasuo*/
    DB::table('types')->insert([
        'id' => '2',
        'name' => 'Blademaster',
        'description' => 'Blademasters have a 35% chance to strike additional times each attack.',
        'updated_at' => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
    ]);  
/*
Blitzcrank
Cho'gath
Rek'Sai
Vi
Volibear
Warwick*/
    DB::table('types')->insert([
        'id' => '3',
        'name' => 'Brawler',
        'description' => 'Brawlers receive bonus maximum health',
        'updated_at' => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
    ]);  
/*Anivia
Brand
Kennen
Lissandra*/
    DB::table('types')->insert([
        'id' => '4',
        'name' => 'Elementalist',
        'description' => 'Elementalists gain double mana from attacks.',
        'updated_at' => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
    ]);  
/*Braum
Leona
Pantheon*/
    DB::table('types')->insert([
        'id' => '5',
        'name' => 'Guardian',
        'description' => 'At the start of combat, all adjacent allies receive +50 armor per stack. Guardians dont buff themselves.',
        'updated_at' => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
    ]);  
/*Gangplank
Graves
Jinx
Lucian
Miss Fortune
Tristana*/
    DB::table('types')->insert([
        'id' => '6',
        'name' => 'Gunslinger',
        'description' => 'After attacking, Gunslingers have a 50% chance to fire additional attacks.',
        'updated_at' => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
    ]);  
/*Darius
Garen
Kayle
Mordekaiser
Poppy
Sejuani*/
    DB::table('types')->insert([
        'id' => '7',
        'name' => 'Knight',
        'description' => 'All allies ignore damage.',
        'updated_at' => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
    ]);  
/*Ashe
Kindred
Varus
Vayne*/
    DB::table('types')->insert([
        'id' => '8',
        'name' => 'Ranger',
        'description' => 'Rangers have a chance to double their attack speed for the next 3s.',
        'updated_at' => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
    ]);  
/*Elise
Gnar
Jayce
Nidalee
Shyvana
Swain*/
    DB::table('types')->insert([
        'id' => '9',
        'name' => 'Shapeshifter',
        'description' => 'Shapeshifters gain bonus maximum Health when they transform.',
        'updated_at' => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
    ]);  
/*Ahri
Aurelion Sol
Karthus
Kassadin
Lulu
Morgana
Twisted Fate
Veigar*/
    DB::table('types')->insert([
        'id' => '10',
        'name' => 'Sorcerer',
        'description' => 'Sorcerers gain double mana from attacking and allies have increased spell damage.',
        'updated_at' => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
    ]);  


}
}