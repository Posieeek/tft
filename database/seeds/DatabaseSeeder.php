<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(OriginsTableSeeder::class);
        $this->call(ChampionsTableSeeder::class);
        $this->call(ChampionOriginTableSeeder::class);
        $this->call(ChampionTypeTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
