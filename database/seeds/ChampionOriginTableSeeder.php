<?php

use Illuminate\Database\Seeder;
use App\Champion;
use App\Origin;

class ChampionOriginTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Shyvana')->first()->id,
            'origin_id' => Origin::where('name', 'Dragon')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Aurelion Sol')->first()->id,
            'origin_id' => Origin::where('name', 'Dragon')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Pantheon')->first()->id,
            'origin_id' => Origin::where('name', 'Dragon')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Aatrox')->first()->id,
            'origin_id' => Origin::where('name', 'Demon')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Brand')->first()->id,
            'origin_id' => Origin::where('name', 'Demon')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Elise')->first()->id,
            'origin_id' => Origin::where('name', 'Demon')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Evelynn')->first()->id,
            'origin_id' => Origin::where('name', 'Demon')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Morgana')->first()->id,
            'origin_id' => Origin::where('name', 'Demon')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Swain')->first()->id,
            'origin_id' => Origin::where('name', 'Demon')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Swain')->first()->id,
            'origin_id' => Origin::where('name', 'Imperial')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Varus')->first()->id,
            'origin_id' => Origin::where('name', 'Demon')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Yasuo')->first()->id,
            'origin_id' => Origin::where('name', 'Exile')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Anivia')->first()->id,
            'origin_id' => Origin::where('name', 'Glacial')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Ashe')->first()->id,
            'origin_id' => Origin::where('name', 'Glacial')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Braum')->first()->id,
            'origin_id' => Origin::where('name', 'Glacial')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Lissandra')->first()->id,
            'origin_id' => Origin::where('name', 'Glacial')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Sejuani')->first()->id,
            'origin_id' => Origin::where('name', 'Glacial')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Volibear')->first()->id,
            'origin_id' => Origin::where('name', 'Glacial')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Camille')->first()->id,
            'origin_id' => Origin::where('name', 'Hextech')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Jayce')->first()->id,
            'origin_id' => Origin::where('name', 'Hextech')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Vi')->first()->id,
            'origin_id' => Origin::where('name', 'Hextech')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Jinx')->first()->id,
            'origin_id' => Origin::where('name', 'Hextech')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Darius')->first()->id,
            'origin_id' => Origin::where('name', 'Imperial')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Draven')->first()->id,
            'origin_id' => Origin::where('name', 'Imperial')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Katarina')->first()->id,
            'origin_id' => Origin::where('name', 'Imperial')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Fiora')->first()->id,
            'origin_id' => Origin::where('name', 'Noble')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Garen')->first()->id,
            'origin_id' => Origin::where('name', 'Noble')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Kayle')->first()->id,
            'origin_id' => Origin::where('name', 'Noble')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Leona')->first()->id,
            'origin_id' => Origin::where('name', 'Noble')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Lucian')->first()->id,
            'origin_id' => Origin::where('name', 'Noble')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Vayne')->first()->id,
            'origin_id' => Origin::where('name', 'Noble')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Akali')->first()->id,
            'origin_id' => Origin::where('name', 'Ninja')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Kennen')->first()->id,
            'origin_id' => Origin::where('name', 'Ninja')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Kennen')->first()->id,
            'origin_id' => Origin::where('name', 'Yordle')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Shen')->first()->id,
            'origin_id' => Origin::where('name', 'Ninja')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Zed')->first()->id,
            'origin_id' => Origin::where('name', 'Ninja')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Karthus')->first()->id,
            'origin_id' => Origin::where('name', 'Phantom')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Kindred')->first()->id,
            'origin_id' => Origin::where('name', 'Phantom')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Mordekaiser')->first()->id,
            'origin_id' => Origin::where('name', 'Phantom')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Gangplank')->first()->id,
            'origin_id' => Origin::where('name', 'Pirate')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Graves')->first()->id,
            'origin_id' => Origin::where('name', 'Pirate')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Miss Fortune')->first()->id,
            'origin_id' => Origin::where('name', 'Pirate')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Pyke')->first()->id,
            'origin_id' => Origin::where('name', 'Pirate')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Twisted Fate')->first()->id,
            'origin_id' => Origin::where('name', 'Pirate')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Blitzcrank')->first()->id,
            'origin_id' => Origin::where('name', 'Robot')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', "Cho'Gath")->first()->id,
            'origin_id' => Origin::where('name', 'Void')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Kassadin')->first()->id,
            'origin_id' => Origin::where('name', 'Void')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', "Kha'Zix")->first()->id,
            'origin_id' => Origin::where('name', 'Void')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', "Rek'Sai")->first()->id,
            'origin_id' => Origin::where('name', 'Void')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Ahri')->first()->id,
            'origin_id' => Origin::where('name', 'Wild')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Gnar')->first()->id,
            'origin_id' => Origin::where('name', 'Wild')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Gnar')->first()->id,
            'origin_id' => Origin::where('name', 'Yordle')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Nidalee')->first()->id,
            'origin_id' => Origin::where('name', 'Wild')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Rengar')->first()->id,
            'origin_id' => Origin::where('name', 'Wild')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Warwick')->first()->id,
            'origin_id' => Origin::where('name', 'Wild')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Lulu')->first()->id,
            'origin_id' => Origin::where('name', 'Yordle')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Poppy')->first()->id,
            'origin_id' => Origin::where('name', 'Yordle')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Tristana')->first()->id,
            'origin_id' => Origin::where('name', 'Yordle')->first()->id,
        ]);

        DB::table('champion_origin')->insert([
            'champion_id' => Champion::where('name', 'Veigar')->first()->id,
            'origin_id' => Origin::where('name', 'Yordle')->first()->id,
        ]);
        //

    }
}
