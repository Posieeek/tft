<?php

use Illuminate\Database\Seeder;

class OriginsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /*Aatrox
Brand
Elise
Evelynn
Morgana
Swain
Varus*/
        DB::table('origins')->insert([
            'id' => '1',
            'name' => 'Demon',
            'description' => 'Demon basic attacks have a 40% chance to burn 20 mana from their target and return some mana to the attacker.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  
/*Aurelion Sol
Pantheon
Shyvana*/
        DB::table('origins')->insert([
            'id' => '2',
            'name' => 'Dragon',
            'description' => 'Dragons are 83% immune to Magic damage',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  
/* Yasuo*/
        DB::table('origins')->insert([
            'id' => '3',
            'name' => 'Exile',
            'description' => ' If an Exile has no adjacent allies at the start of combat, they gain a shield equal to a 100% max health.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  
/*Anivia
Ashe
Braum
Lissandra
Sejuani
Volibear*/
        DB::table('origins')->insert([
            'id' => '4',
            'name' => 'Glacial',
            'description' => 'On hit, Glacials have a chance to stun for 2s.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  
/*Camille
Jayce
Vi
Jinx*/
        DB::table('origins')->insert([
            'id' => '5',
            'name' => 'Hextech',
            'description' => 'Throw a bomb at an enemy unit with an item, and disables all items in a hex radius for 7 seconds.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  
        /*Darius
Draven
Katarina
Swain*/
        DB::table('origins')->insert([
            'id' => '6',
            'name' => 'Imperial',
            'description' => 'Imperials deal Double Damage.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  

/*
Fiora
Garen
Kayle
Leona
Lucian
Vayne*/
        DB::table('origins')->insert([
            'id' => '7',
            'name' => 'Noble',
            'description' => '+50 Armor and Magic Resist. Heal 25 health per attack.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  
/*Akali
Kennen
Shen
Zed*/
        DB::table('origins')->insert([
            'id' => '8',
            'name' => 'Ninja',
            'description' => 'The Ninja Trait is only active when you have exactly 1 or 4 Ninjas.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  

      /*  Karthus
        Kindred
        Mordekaiser
        */

        DB::table('origins')->insert([
            'id' => '9',
            'name' => 'Phantom',
            'description' => 'Curse a random enemy at the start of combat, setting their HP to 100.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  
    
        /*Gangplank
Graves
Miss Fortune
Pyke
Twisted Fate*/


        DB::table('origins')->insert([
            'id' => '10',
            'name' => 'Pirate',
            'description' => 'Earn up to 4 additional gold after combat against another player.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  
/*Blitzcrank*/
        DB::table('origins')->insert([
            'id' => '11',
            'name' => 'Robot',
            'description' => 'Robots start combat at full mana.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]);  
/*Cho'gath
Kassadin
Kha'Zix
Rek'Sai*/
        DB::table('origins')->insert([
            'id' => '12',
            'name' => 'Void',
            'description' => 'Void units now deal True Damage.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]); 
/*Ahri
Gnar
Nidalee
Rengar
Warwick*/

        DB::table('origins')->insert([
            'id' => '13',
            'name' => 'Wild',
            'description' => 'Attacks generate stacks of Fury (stacks up to 5 times). Each stack of Fury gives 12% Attack Speed.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]); 
/*
Gnar
Kennen
Lulu
Poppy
Tristana
Veigar*/

        DB::table('origins')->insert([
            'id' => '14',
            'name' => 'Yordle',
            'description' => 'Attack against ally Yordles have a chance to miss. Also dodges On-hit effects.',
            'updated_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
        ]); 

    }
}
