<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Champion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     *
     * @var array
     */
    protected $guarded = [];

    public function origins()
    {
        return $this->belongsToMany(Origin::class);
    }

    public function types()
    {
        return $this->belongsToMany(Type::class);
    }

    public function stats()
    {
        return $this->belongsToOne(Stat::class);
    }
}
