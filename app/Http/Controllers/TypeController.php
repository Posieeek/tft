<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Origin;

class TypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    /*
    public function index()
    {
        return view('/types/index');
    }
     */



    public function show($name)
    {
        $type = Type::where('name', $name)->with('champions')->first();
        return view('types.show', compact('type'));
    }


}
