<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Origin;
use App\Type;

class OriginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     *//*
    public function index()
    {
        return view('/origins/index');
    }*/

    public function show($name)
    {
        $origin = Origin::where('name', $name)->with('champions')->first();
        return view('origins.show', compact('origin'));
    }


}
