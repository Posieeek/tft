<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Champion;
use App\Stat;
use App\Type;
use App\Origin;


class ChampionController extends Controller

{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sortBy = request('sort_by');
        $sortDir = request('sort_dir');
       // $sortPage = request('sort_page');

        if ($sortBy) {
            $champions = Champion::orderBy($sortBy, $sortDir ?? 'asc')->paginate(20);
        } else {
            $champions = Champion::paginate(20);
        }

        return view('/champions/index', compact('champions'));
    }

    public function show($name)
    {
      //  $type = Type::where('name', $name)->with('champions')->first();

        $champion = Champion::where('name', $name)->first();

        return view('champions.show', compact('champion'));
    }
}
