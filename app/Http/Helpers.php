<?php

if (!function_exists('handleSorting')) {
    function handleSorting()
    {
        return request('sort_dir') === 'desc' ? 'asc' : 'desc';
    }
}
