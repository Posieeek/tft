@if ($errors->has($what))
    <span class="invalid-feedback"><strong>{{ $errors->first($what) }}</strong></span>
@endif