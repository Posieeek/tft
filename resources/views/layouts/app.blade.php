<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'GymApp') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>

    <div id="app">
        <nav class="navbar  navbar-light navbar-laravel navbar-left">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('Home', 'Home') }}
                </a> @php $types = \App\Type::all(); $origins = \App\Origin::all();
                @endphp

                <div class="dropdown">
                    <a href="{{route('champions.index')}}" class="navbar-brand" style="color:white">Champions</a>

                </div>

                <div class=" dropdown ">
                    <a class="navbar-brand " style="color:white ">Classes</a>
                    <div class="dropdown-content ">
                        @foreach ($types as $type)
                        <a href="{{ route( 'types.show', [ 'type'=> $type->name]) }}">{{$type->name}}</a> @endforeach
                    </div>
                </div>


                <div class="dropdown">
                    <a class="navbar-brand" style="color:white">Origins</a>
                    <div class="dropdown-content">
                        @foreach ($origins as $origin)
                        <a href="{{ route('origins.show', ['type' => $origin->name]) }}">{{$origin->name}}</a>
                        @endforeach
                    </div>
                </div>

                <div class="dropdown">
                    <a class="navbar-brand" style="color:white">Team Builder</a>
                </div>

                @auth
                <div class="dropdown">
                    <a class="navbar-brand" style="color:white">Profile</a>
                    <div class="dropdown-content">

                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Wyloguj') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
                @endauth @guest

                <div class="dropdown">
                    <a class="navbar-brand" style="color:white">Login</a>
                    <div class="dropdown-content">
                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                        <a href="{{ route('register') }}">{{ __('Register') }}</a>
                    </div>
                </div>

                @endguest
            </div>
        </nav>

        <div class="container-fluid">
            <div id="elo"></div>
            @yield('content')
        </div>
    </div>

</body>

</html>