
@foreach([
    'success' => 'success', 'confirmation-success' => 'success',
    'status' => 'info',
    'error' => 'danger', 'confirmation-danger' => 'danger'] as $type => $class)
    @if( Session::has( $type ) )
        <div id="alert" class="alert alert-{{$class}} alert-dismissible fade show" role="alert">
            <span class="alert-text mr-2">{!! Session::get( $type ) !!}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
            </button>
        </div>
    @endif
@endforeach
