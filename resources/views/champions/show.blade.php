@extends('layouts.app') 
@section('content') @if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="container emp-profile-small">
    <form method="post">
        <div class="row">
            <div class="col-md-3">
                <div class="profile-img">
                    <img src="/img/lol_avatars/{{ $champion->avatar }}" style="width:160px; height:160px">


                </div>
            </div>

            <div class="col-md-7">



                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Info</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Ultimate</a>
                    </li>
                </ul>

            </div>
            <div class="col-md-2">

                <a href="{{ route('champions.index') }}" class="profile-edit-btn" style="margin-right: 8px;">Back</a>

            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="profile-work">
                    <h1>{{$champion->name}}</h1>


                </div>
            </div>
            <div class="col-md-9">
                <div class="tab-content profile-tab" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Health</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$champion->health}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Armor</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$champion->armor}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Mag resist</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$champion->mr}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Value</label>
                            </div>

                            <div class="col-md-6">
                                <p>{{$champion->price}} <i class="fas fa-coins"></i></p>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <label>DPS</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$champion->dps}}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label>Class</label>
                            </div>
                            <div class="col-md-6">
                                @foreach ($champion->types as $type)
                                <p>
                                    <a href="{{ route('types.show', ['type' => $type->name]) }}">{{$type->name}}
                                    </a>
                                </p>
                                @endforeach
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label>Origin</label>
                            </div>
                            <div class="col-md-6">
                                @foreach ($champion->origins as $origin)
                                <p>
                                    <a href="{{ route('origins.show', ['origin' => $origin->name]) }}">{{$origin->name}}
                                    </a>
                                </p> @endforeach
                            </div>
                        </div>





                    </div>


                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                        <div class="row">
                            <div class="col-md-9">
                                <p>{{$champion->description}}</p>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </form>
    <br>
    <hr>

</div>
@endsection