@extends('layouts.app') 
@section('content')
<div class="homepage">
    <div class="content">
        <div class="container emp-profile">
            <div class="col-sm-12 ">
                <div class="card">
                    <div class="row justify-content-center">
                        <div class="col-sm-10 ">
                            <div class="card-body">
                                <div class="title m-b-md">

                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover table-condensed">
                                        <th>Avatar </th>
                                        <th>
                                            <a href="{{route('champions.index', ['sort_by' => 'Name', 'sort_dir' => handleSorting()])}}">Champion</a>
                                        </th>
                                        <th>
                                            <a href="{{route('champions.index', ['sort_by' => 'Health', 'sort_dir' => handleSorting()])}}">Health</a>
                                        </th>
                                        <th>
                                            <a href="{{route('champions.index', ['sort_by' => 'Armor', 'sort_dir' => handleSorting()])}}">Armor</a>
                                        </th>
                                        <th>
                                            <a href="{{route('champions.index', ['sort_by' => 'Mr', 'sort_dir' => handleSorting()])}}">Mag Resist</a>
                                        </th>
                                        <th>
                                            <a href="{{route('champions.index', ['sort_by' => 'DPS', 'sort_dir' => handleSorting()])}}">DPS</a>
                                        </th>
                                        <th>
                                            <a href="{{route('champions.index', ['sort_by' => 'Price', 'sort_dir' => handleSorting()])}}">Value</a>
                                        </th>

                                        @foreach ($champions as $champion)
                                        <tr class="noBorder">

                                            <td>
                                                <a href="{{ route('champions.show', ['champion' => $champion->name]) }}">
                                                    <img src="/img/lol_avatars/{{ $champion->avatar }}"
                                                        style="width:60px; height:60px">
                                                </a>
                                            </td>

                                            <td>{{ $champion->name}}</td>
                                            <td>{{ $champion->health}}</td>
                                            <td>{{ $champion->armor}}</td>
                                            <td>{{ $champion->mr}}</td>
                                            <td>{{ $champion->dps}}</td>
                                            <td>{{ $champion->price}} <i class="fas fa-coins"></i></td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    {{-- {{ $champions->links() }} --}} {{ $champions->appends(request()->except('page'))->render() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection