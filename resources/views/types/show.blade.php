@extends('layouts.app') 
@section('content')
<div class="homepage">
    <div class="content">
        <div class="container emp-profile">
            <div class="col-sm-12 ">
                <div class="card">
                    <div class="row justify-content-center">
                        <div class="col-sm-10 ">
                            <div class="card-body">
                                <div class="title m-b-md">
                                    {{$type->description}}
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover table-condensed">
                                        <th>Avatar </th>
                                        <th>Champion</th>
                                        <th>Description</th>
                                        <th>Value</th>

                                        @foreach($type->champions as $champion)
                                        <tr class="noBorder">

                                            <td>
                                                <a href="{{ route('champions.show', ['champion' => $champion->name]) }}">
                                                <img src="/img/lol_avatars/{{ $champion->avatar }}" style="width:60px; height:60px">
                                            </a>
                                            </td>

                                            <td>{{ $champion->name}}</td>
                                            <td class="description">{{ $champion->description}}</td>
                                            <td>{{ $champion->price}} <i class="fas fa-coins"></i></td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection