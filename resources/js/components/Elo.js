import React, { Component } from "react";
import ReactDOM from "react-dom";

export default class Elo extends Component {
    state = {
        counter: 0
    };

    componentDidMount() {
        setInterval(() => {
            this.setState(state => ({ counter: state.counter + 1 }));
        }, 1000);
    }

    render() {
        return (
            <div className="container mt-2">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">
                                <h1 class="display-1 text-center">
                                    {this.state.counter}
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById("elo")) {
    ReactDOM.render(<Elo />, document.getElementById("elo"));
}
