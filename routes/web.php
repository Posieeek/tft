<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*
Route::get('/types/{type}', 'TypeController@show')->name('types.show');
 */
Route::group(['prefix' => 'types', 'as' => 'types.'], function () {
 //   Route::get('/', 'TypeController@index')->name('index');
    Route::get('/{name}', 'TypeController@show')->name('show');
});

Route::group(['prefix' => 'origins', 'as' => 'origins.'], function () {
 //   Route::get('/', 'OriginController@index')->name('index');
    Route::get('/{name}', 'OriginController@show')->name('show');
});

Route::group(['prefix' => 'champions', 'as' => 'champions.'], function () {
    Route::get('/', 'ChampionController@index')->name('index');
    Route::get('/{name}', 'ChampionController@show')->name('show');
});